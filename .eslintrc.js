module.exports = {
	root: true,
	extends: ['@beezyinc/eslint-config-beezy', 'plugin:@typescript-eslint/recommended'],
	globals: {
		_spPageContextInfo: true,
		beezyContext: true,
		beezySettings: true,
		dataAssetsUrl: true,
		dataCurrentSiteUrl: true,
		define: true,
		window: true
	},
	ignorePatterns: ['.eslintrc.js'],
	parser: '@typescript-eslint/parser',
	parserOptions: {
		project: './tsconfig.json',
		tsconfigRootDir: __dirname
	},
	plugins: ['@typescript-eslint'],
	rules: {
		'@typescript-eslint/no-empty-function': 'warn',
		'class-methods-use-this': 'off',
		'consistent-return': 'warn',
		'import/no-amd': 'off',
		'import/no-extraneous-dependencies': 'off',
		'import/no-unresolved': 'off',
		'no-extend-native': ['error', { exceptions: ['String'] }],
		'no-underscore-dangle': ['error', { allow: ['_spPageContextInfo', '_topPlaceholder', '_onDispose'] }],
		'react/prefer-stateless-function': 'warn'
	},
	settings: {
		'import/resolver': {
			node: {
				extensions: ['.js', '.jsx', '.ts', '.tsx', '.scss']
			}
		}
	}
};
