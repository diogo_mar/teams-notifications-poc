interface activityConfig {
	userId: string;
	activityType: ActivityType;
	templateParameters: { name: string; value: string }[];
	textPreview?: string;
	/* this value will be passed to the subEntityId property in the teams context of the target app */
	subEntityId?: string;
}

export enum ActivityType {
	DirectNotification = "DirectNotification",
	Poke = "Poke",
}

const getActivityBody = (config: activityConfig) => {
	const { subEntityId, textPreview } = config;
	const context = subEntityId && { subEntityId: config.subEntityId };
	const contextParam = context
		? encodeURIComponent(`context=${context}`)
		: "";
	const teamsManifestId =
		process.env.NODE_ENV === "development"
			? "46f3c2be-1f43-4932-af3c-ec632c2b8f73"
			: "c9baa253-f840-4385-a3c8-a84b6326306b";

	return {
		endpoint: `/users/${config.userId}/teamwork/sendActivityNotification`,
		method: "POST",
		body: {
			topic: {
				source: "text",
				value: "TeamsNotifications",
				webUrl: `https://teams.microsoft.com/l/entity/${teamsManifestId}/index?${contextParam}`,
			},
			activityType: config.activityType,
			previewText: textPreview && {
				content: config.textPreview,
			},
			templateParameters: config.templateParameters,
		},
	};
};

export default getActivityBody;
