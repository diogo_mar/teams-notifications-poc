import {
	Button,
	Flex,
	Input,
	Text,
} from "@fluentui/react-northstar";
import { IDynamicPerson } from "@microsoft/mgt-react";
import {
	getResourceConfiguration,
	ResourceType,
	TeamsUserCredential,
	UserInfo,
} from "@microsoft/teamsfx";
import { FC, useCallback, useEffect, useState } from "react";
import getActivityBody, { ActivityType } from "./lib/getActivityBody";
import PeoplePicker from "./PeoplePicker";


const Notifications: FC<{}> = () => {
	const [teamsUser, setTeamsUser] = useState<UserInfo>();
	const [accessToken, setAccessToken] = useState<string>();
	const [selected, setSelected] = useState<IDynamicPerson[]>([]);
	const [message, setMessage] = useState<string>("");
	const [error, setError] = useState<unknown>();
	const [sending, setSending] = useState<boolean>(false);

	useEffect(() => {
		const credential = new TeamsUserCredential();
		const userInfoReq = credential.getUserInfo();
		const accessTokenReq = credential.getToken("");
		const reqPromises = [userInfoReq, accessTokenReq] as const;

		Promise.all(reqPromises)
			.then(([userInfo, accessToken]) => {
				setTeamsUser(userInfo);
				setAccessToken(accessToken?.token);
			})
			.catch((err) => {
				setError("Failed to retrieve Teams credentials");
			});
	}, []);

	const notifyUser = useCallback(
		async (targetUser: IDynamicPerson, message: string) => {
			if (!targetUser.id || !teamsUser || !accessToken) {
				return;
			}

			const apiConfig = getResourceConfiguration(ResourceType.API);
			const functionName = "notifyUser";
			const functionUrl = `${apiConfig.endpoint}/api/${functionName}`;

			const notificationBody = getActivityBody({
				userId: targetUser.id,
				activityType: ActivityType.DirectNotification,
				templateParameters: [
					{ name: "user", value: teamsUser.displayName },
					{ name: "message", value: message },
				],
			});

			return fetch(functionUrl, {
				body: JSON.stringify(notificationBody),
				method: "POST",
				headers: {
					"content-type": "application/json",
					authorization: "Bearer " + accessToken || "",
				},
			});
		},
		[teamsUser, accessToken]
	);

	const handleChange = useCallback((value: IDynamicPerson[]) => {
		setSelected(value);
	}, []);

	const handleClick = useCallback(async () => {
		setSending(true);
		try {
			const promises = selected.map((user) => {
				return notifyUser(user, message);
			});
			await Promise.all(promises);
			setError(undefined);
		} catch (err) {
			setError(err);
		} finally {
			setSending(false);
		}
	}, [selected, message, notifyUser]);

	return (
		<Flex
			column
			fill
			gap="gap.large"
			hAlign="start"
			styles={{ maxWidth: "400px", margin: "0 auto" }}
		>
			<PeoplePicker onChange={handleChange} />
			<Input
				fluid
				label="Type a message to send in the notification"
				placeholder="Message text"
				onChange={(e, data) => setMessage(data?.value || "")}
			></Input>

			<Button
				primary
				disabled={!selected?.length || !message?.trim() || sending}
				onClick={handleClick}
			>
				{sending ? "Sending..." : "Notify"}
			</Button>
			{error && <Text color="red" content={error + ""} />}
		</Flex>
	);
};

export default Notifications;
