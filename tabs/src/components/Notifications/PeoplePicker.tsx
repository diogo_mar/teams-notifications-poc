import { FC, useCallback, useEffect, useState } from "react";
import { Client, GraphError } from "@microsoft/microsoft-graph-client";
import { useGraph } from "../sample/lib/useGraph";
import {
	Button,
	Dropdown,
	DropdownItemProps,
	Loader,
	ShorthandCollection,
	Text,
} from "@fluentui/react-northstar";
import { IDynamicPerson } from "@microsoft/mgt-react";
import { setTimeout } from "timers";

export interface PeoplePickerProps {
	onChange?: (people: IDynamicPerson[]) => any;
}

type IDynamicPersonItems = ShorthandCollection<
	DropdownItemProps,
	IDynamicPerson
>;
const SCOPES = ["People.Read", "User.Read.All"];

const PeoplePicker: FC<PeoplePickerProps> = ({
	onChange,
}: PeoplePickerProps) => {
	const [selected, setSelected] = useState<IDynamicPerson[]>([]);
	const [users, setUsers] = useState<IDynamicPerson[]>([]);
	const [dropdownItems, setDropdownItems] = useState<IDynamicPersonItems>([]);
	const [graphClient, setGraphClient] = useState<Client | undefined>();
	const [searchQuery, setSearchQuery] = useState("");
	const [loadingQuery, setLoadingQuery] = useState(false);
	const graphQuery = useCallback(
		async (graph: Client) => {
			let users: IDynamicPerson[] = [];
			try {
				setLoadingQuery(true);
				const query = searchQuery
					? `/users?$filter=startsWith(displayName,'${searchQuery}') or startsWith(surname,'${searchQuery}') or startsWith(givenName,'${searchQuery}')`
					: `/me/people?filter=personType/class eq 'Person' and personType/subclass eq 'OrganizationUser'`;
				const response = await graph.api(query).get();

				setGraphClient(graph);
				users = response.value;
			} catch (error) {
				if (error instanceof GraphError && error.code === "401") {
					setGraphClient(undefined);
				}
				throw error;
			} finally {
				setUsers(users);
				setLoadingQuery(false);
				return users;
			}
		},
		[searchQuery]
	);
	const { loading, reload } = useGraph(graphQuery, { scope: SCOPES });

	// fire change event
	useEffect(() => {
		if (onChange) {
			onChange(selected);
		}
	}, [onChange, selected]);

	// requery graph api
	useEffect(() => {
		if (graphClient) {
			const timeoutId = setTimeout(() => {
				graphQuery(graphClient);
			}, 300);
			return function cleanup() {
				clearTimeout(timeoutId);
			};
		}
	}, [graphQuery, graphClient]);

	// map dropdown items with api results
	useEffect(() => {
		setDropdownItems(
			users?.map<DropdownItemProps>((person) => {
				return {
					key: person.id,
					header: person.displayName,
					image: person.personImage,
					onClick: (e) => {
						handleClick(person);
					},
				};
			}) || []
		);
	}, [users]);

	const handleClick = (person: IDynamicPerson) => {
		setSelected((oldSelected) => {
			let newSelected: IDynamicPerson[];

			if (!oldSelected.find((item) => item.id === person.id)) {
				newSelected = [...oldSelected, person];
			} else {
				newSelected = oldSelected.filter(
					(item) => item.id !== person.id
				);
			}
			return newSelected;
		});
	};

	return loading ? (
		<Loader size="large"></Loader>
	) : !graphClient ? (
		<>
			<span>
				Click to authorize this app to read your contacts using
				Microsoft Graph.
			</span>
			<Button onClick={reload} content="Authorize" />
		</>
	) : (
		<div>
			<Text content="Start typing a name to choose notification targets" />
			<Dropdown
				placeholder="Notification targets"
				search
				multiple
				loading={loadingQuery}
				loadingMessage="Loading results..."
				onSearchQueryChange={(e, data) =>
					setSearchQuery(data.searchQuery || "")
				}
				items={dropdownItems}
			></Dropdown>
		</div>
	);
};

export default PeoplePicker;
